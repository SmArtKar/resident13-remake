/dialogue/npc/resident/quest/sarah

/dialogue/npc/resident/quest/sarah/get_dialogue_options(var/mob/living/advanced/player/P,var/list/known_options)

	. = ..()

	.["hello"] = list(
		"Если у тебя не что-то срочное, то пожалуйста, не отвлекай меня. Хорошо?",
		"Вылазка",
		"Задания"
	)

	.["Вылазка"] = list(
		"Я не сказала бы, что кто-либо из вас готов к очередному выходу за пределы лагеря, однако особого выбора у нас нет. Ваши раны, в принципе, не смертельны, так что справитесь. \
		В любом случае, ваше состояие куда лучше, чем у большинства тут лежащих.",
		"Пострадавшие"
	)

	.["Задания"] = list(
		"Сейчас, единственное, чем вы действительно можете помочь мне и всем остальным - медикаментами. А их можно достать лишь за кордоном. \
		Если мне не изменяет память - именно туда Стуков хотел послать первую группу разведки, но...",
		"Медикаменты"
	)

	.["Пострадавшие"] = list(
		"Практически весь боевой состав, несколько обычных гражданских. Вся команда разведки тоже отлетела...Чёрт, я не представляю, как справиться со всем этим в одиночку.",
		"Задания"
	)

	.["Медикаменты"] = list(
		"Ядро, находящееся в лагере - отлично защищает всех нас от вируса. В своём первозданном виде. Однако, беря в расчёт то, что каждый из заражённых испаряется после смерти, \
		выпуская в воздух почти двойную дозу К-04...В общем, от десятикратной дозировки нас не защитил даже этот артефакт, из-за чего, \
		большая часть добровольцев, находившихся на передовой во время прихода орды - оказалась заражена. Кто-то обратился за считанные минуты или часы...Этим повезло больше. \
		Их организм всё ещё борется с газом, однако долго он делать это не сможет. Под городом, в районе библиотеки, должен быть старый медицинский склад, с запасами антибиотиков. \
		Они не вылечат их, но помогут продержаться как минимум ещё пару недель. Это лучшее, что мы можем для них сделать."
	)

	return .