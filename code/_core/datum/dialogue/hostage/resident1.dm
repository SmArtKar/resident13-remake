/dialogue/hostage/res/


/dialogue/hostage/res/get_dialogue_options(var/mob/living/advanced/player/P,var/list/known_options)

	. = ..()

	var/mob/living/advanced/A = P.dialogue_target

	if(istype(A) && A.handcuffed)
		.["hello"] = list(
			"Что?...Спасение? Поверить не могу! Скорее сними с меня эти #1",
			"наручники",
			"*жди тут",
			"*следуй за мной"
		)
	else
		.["hello"] = list(
			pick("Как дела?","Как настроение?","Тут холодно..."),
			"*жди тут",
			"*следуй за мной"
		)

	.["наручники"] = list(
		"Просто сними их уже!"
	)

	return .