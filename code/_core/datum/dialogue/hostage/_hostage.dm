/dialogue/hostage/get_dialogue_options(var/mob/living/advanced/player/P,var/list/known_options)

	. = list()

	.["*жди тут"] = list(
		"Подождём."
	)

	.["*следуй за мной"] = list(
		"Двинули."
	)

	return .


/dialogue/hostage/set_topic(var/mob/living/advanced/player/P,var/topic)

	. = ..()

	if(!is_living(P.dialogue_target))
		return .

	var/mob/living/L = P.dialogue_target

	if(!L.ai)
		return .

	switch(topic)
		if("*жди тут")
			L.ai.set_move_objective(null)
		if("*следуй за мной")
			L.ai.set_move_objective(P,TRUE)

	return .