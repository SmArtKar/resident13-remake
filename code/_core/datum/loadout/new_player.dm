/loadout/new_player
	spawning_items = list(
		/obj/item/clothing/underbottom/underwear/boxers,
		/obj/item/clothing/undertop/underwear/shirt,
		/obj/item/clothing/feet/socks/ankle,
		/obj/item/clothing/feet/socks/ankle,
		/obj/item/clothing/feet/shoes/black_boots,
		/obj/item/clothing/feet/shoes/black_boots/left,
		/obj/item/clothing/back/storage/satchel
	)

/loadout/new_player/get_spawning_items(var/mob/living/advanced/A,var/obj/item/I)
	. = ..()
	. += pick(/obj/item/clothing/uniform/res/destro_scientist,/obj/item/clothing/uniform/res/civ,/obj/item/clothing/uniform/res/civ2)
	return .

/loadout/new_player/female
	spawning_items = list(
		/obj/item/clothing/underbottom/underwear/panty,
		/obj/item/clothing/undertop/underwear/bra,
		/obj/item/clothing/feet/socks/knee/white,
		/obj/item/clothing/feet/socks/knee/white,
		/obj/item/clothing/uniform/res/destro_scientist,
		/obj/item/clothing/feet/shoes/black_boots,
		/obj/item/clothing/feet/shoes/black_boots/left,
		/obj/item/clothing/back/storage/satchel
	)