/species/zombie/
	name = "Zombie"
	desc = "RUN FOR YOUR LIFE!"
	id = "zombie"
	flags_species = SPECIES_HUMAN

	flags_chargen = CHARGEN_SEX

	default_color_eye = "#FFFFFF"
	default_blood_color = "#e4e4e4"
	default_color_skin = "#FFFFFF"
	default_color_hair = "#FFFFFF"
	default_color_detail = "#FFFFFF"
	default_color_glow = "#FFFFFF"

	spawning_organs_male = list(
		BODY_TORSO = /obj/item/organ/torso/zombie,
		BODY_HEAD = /obj/item/organ/head/zombie,
		BODY_GROIN = /obj/item/organ/groin/zombie,
		BODY_LEG_RIGHT = /obj/item/organ/leg/zombie,
		BODY_LEG_LEFT = /obj/item/organ/leg/zombie/left,
		BODY_FOOT_RIGHT = /obj/item/organ/foot/zombie,
		BODY_FOOT_LEFT = /obj/item/organ/foot/zombie/left,
		BODY_ARM_RIGHT = /obj/item/organ/arm/zombie,
		BODY_ARM_LEFT = /obj/item/organ/arm/zombie/left,
		BODY_HAND_RIGHT = /obj/item/organ/hand/zombie,
		BODY_HAND_LEFT = /obj/item/organ/hand/zombie/left,
		BODY_EYE_RIGHT = /obj/item/organ/eye/zombie,
		BODY_EYE_LEFT = /obj/item/organ/eye/zombie/left,
		BODY_EAR_RIGHT = /obj/item/organ/ear,
		BODY_EAR_LEFT = /obj/item/organ/ear/left,

		BODY_BRAIN = /obj/item/organ/internal/brain,
		BODY_HEART = /obj/item/organ/internal/heart,
		BODY_LUNGS = /obj/item/organ/internal/lungs,
		BODY_STOMACH = /obj/item/organ/internal/stomach,
		BODY_LIVER = /obj/item/organ/internal/liver,
		BODY_INTESTINTES = /obj/item/organ/internal/intestines,
		BODY_KIDNEYS = /obj/item/organ/internal/kidneys

	)

	spawning_organs_female = list(
		BODY_TORSO = /obj/item/organ/torso/zombie/female,
		BODY_HEAD = /obj/item/organ/head/zombie/female,
		BODY_GROIN = /obj/item/organ/groin/zombie/female,
		BODY_LEG_RIGHT = /obj/item/organ/leg/zombie,
		BODY_LEG_LEFT = /obj/item/organ/leg/zombie/left,
		BODY_FOOT_RIGHT = /obj/item/organ/foot/zombie,
		BODY_FOOT_LEFT = /obj/item/organ/foot/zombie/left,
		BODY_ARM_RIGHT = /obj/item/organ/arm/zombie,
		BODY_ARM_LEFT = /obj/item/organ/arm/zombie/left,
		BODY_HAND_RIGHT = /obj/item/organ/hand/zombie,
		BODY_HAND_LEFT = /obj/item/organ/hand/zombie/left,
		BODY_EYE_RIGHT = /obj/item/organ/eye/zombie,
		BODY_EYE_LEFT = /obj/item/organ/eye/zombie/left,
		BODY_EAR_RIGHT = /obj/item/organ/ear,
		BODY_EAR_LEFT = /obj/item/organ/ear/left,

		BODY_BRAIN = /obj/item/organ/internal/brain,
		BODY_HEART = /obj/item/organ/internal/heart,
		BODY_LUNGS = /obj/item/organ/internal/lungs,
		BODY_STOMACH = /obj/item/organ/internal/stomach,
		BODY_LIVER = /obj/item/organ/internal/liver,
		BODY_INTESTINTES = /obj/item/organ/internal/intestines,
		BODY_KIDNEYS = /obj/item/organ/internal/kidneys
	)