obj/structure/interactive/misc/
	icon = 'icons/obj/structure/misc.dmi'
	plane = -6

obj/structure/interactive/misc/tv
	name = "tv"
	desc = "Don't sit too close to the television!"
	desc_extended = "A vintage television as decor to liven up the area."
	icon_state = "tv"
	collision_flags = FLAG_COLLISION_WALL
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC
	plane = PLANE_WALL

	bullet_block_chance = 25


obj/structure/interactive/misc/dresser
	name = "dresser"
	desc = "Undie dispenser."
	desc_extended = "A clothing dresser as decor to liven up the area. Doesn't actually dispense underwear, sadly."
	icon_state = "dresser"
	collision_flags = FLAG_COLLISION_WALL
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	bullet_block_chance = 50

obj/structure/interactive/misc/dresser/chargen/clicked_on_by_object(caller,object,location,control,params)

	if(!is_advanced(caller))
		return ..()

	INTERACT_CHECK

	var/mob/living/advanced/A = caller

	if(length(A.worn_objects))
		return TRUE

	A.equip_loadout(/loadout/new_player,TRUE)


	return TRUE


obj/structure/interactive/misc/toilet
	name = "toilet"
	desc = "*obligatory poop joke*"
	desc_extended = "A toilet as decor to liven up the area. Warranty void if used for swirlies."
	icon_state = "toilet"

obj/structure/interactive/misc/urinal
	name = "urinal"
	desc = "*obligatory piss joke*"
	desc_extended = "A urinal on the wall as decor to liven up the area."
	icon_state = "urinal"
	plane = PLANE_WALL_ATTACHMENTS



obj/structure/interactive/misc/shower
	name = "shower"
	desc = "For when you don't feel like taking a bath."
	desc_extended = "A shower fixture as decor to liven up the area."
	icon_state = "shower"

obj/structure/interactive/misc/mirror
	name = "mirror"
	desc = "Magic mirror on the wall, who is the most robust of them all?"
	desc_extended = "Stand in front of this to change your appearance."
	icon_state = "mirror"
	plane = PLANE_WALL_ATTACHMENTS


obj/structure/interactive/misc/mirror/chargen/Crossed(var/atom/movable/O,var/atom/new_loc,var/atom/old_loc)
	if(is_player(O))
		var/mob/living/advanced/player/P = O
		P.add_chargen_buttons()
		P.handle_hairstyle_chargen(-1,update_blends=FALSE)
		P.handle_beardstyle_chargen(-1,update_blends=FALSE)
		P.update_all_blends()
		P.show_hud(TRUE,FLAGS_HUD_CHARGEN,FLAGS_HUD_SPECIAL,speed=3)

	return ..()

obj/structure/interactive/misc/mirror/chargen/Uncrossed(var/atom/movable/O,var/atom/new_loc,var/atom/old_loc)
	if(is_advanced(O))
		var/mob/living/advanced/player/P = O
		P.remove_chargen_buttons()
		//P.save()
	return ..()

obj/structure/interactive/misc/curtain_open
	name = "curtain"
	desc = "For warding off peeping toms."
	desc_extended = "A curtain attached to the wall/ceiling as decor to liven up the area."
	icon_state = "curtain_open"
	alpha = 175

obj/structure/interactive/misc/sink
	name = "sink"
	desc = "Wa'tr. Wa'tr free o' charge."
	desc_extended = "A standing sink as decor to liven up the area. What? You actually expected this to dispense water free of charge?"
	icon_state = "sink"

/*obj/structure/interactive/misc/sink/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller

	if(prob(50))
		caller.add_hydration(2*.*2,FALSE)
		caller.health.adjust_tox_loss(10)
		caller.visible_message(span("notice","[caller.name] пьёт из крана"))
	else
		P.to_chat(span("notice","Воды нет...Не повезло."))*/

obj/structure/interactive/misc/resident
	name = "Аче"
	desc = "Аче)))"
	desc_extended = ")))))"
	icon = 'icons/stalker/metro-2/decor.dmi'
	icon_state = "100_rentgen"

obj/structure/interactive/misc/resident/grille
	name = "Решётка"
	desc = "Ржавая решётка."
	desc_extended = "Старая металлическая решётка, ничего особо интересного."
	icon = 'icons/stalker/lohweb/structures.dmi'
	icon_state = "bars"
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 10

obj/structure/interactive/misc/resident/grille2
	name = "Решётка"
	desc = "Ржавая решётка."
	desc_extended = "Старая металлическая решётка, ничего особо интересного."
	icon = 'icons/stalker/lohweb/structures.dmi'
	icon_state = "forcedoor0"
	collision_flags = FLAG_COLLISION_WALL

obj/structure/interactive/misc/resident/stop
	name = "Дорожный знак"
	desc = "Бесполезный кусок металла."
	desc_extended = "Старый, уже поржавевший дорожный знак, предупреждающий о опасности впереди."
	icon_state = "stop_sign"
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 10

obj/structure/interactive/misc/resident/yashik_green
	name = "Деревянный ящик"
	desc = "Зелёный деревянный ящик."
	desc_extended = "Хорошо сохранившийся деревянный ящик, покрашенный в зелёный. Имеет несколько металлических стержней и пластин, установленных специально для того, чтобы помешать вскрытию."
	icon_state = "yashik_a"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALKING
	bullet_block_chance = 30

obj/structure/interactive/misc/resident/big_yashik
	name = "Большой ящик"
	desc = "Большой деревянный ящик."
	desc_extended = "Хорошо сохранившийся, метровый ящик с металлическими стержнями и пластинами. Такой никак не вскроешь."
	icon_state = "crate"
	icon = 'icons/stalker/decorations_32x64.dmi'
	plane = -5
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 60

obj/structure/interactive/misc/resident/yashik
	name = "Сгнивший деревянный ящик"
	desc = "Ящик."
	desc_extended = "Сильно потрёпанный, да ещё и сгнивший практически по всей площади ящик, который, при должной сноровке - должно быть достаточно легко сломать."
	icon_state = "yashik"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALKING
	bullet_block_chance = 30

obj/structure/interactive/misc/resident/shkaf
	name = "Шкаф"
	desc = "Большой деревянный шкаф."
	desc_extended = "Огромный по своим размерам деревянный шкаф, способный вместить в себя до двух человек минимум. Жаль только, что абсолютно пустой."
	icon_state = "shkaf64"
	icon = 'icons/stalker/decorations_32x64.dmi'
	plane = PLANE_WALL
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 60

obj/structure/interactive/misc/resident/grey_bochka
	name = "Металлическая бочка"
	desc = "Серая металлическая бочка."
	desc_extended = "Сильно поржавевший бочок, окрашенный в серый цвет. Выглядит красиво и придаёт местности своей эстетики, но абсолютно бесполезен."
	icon_state = "bochka"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALKING
	bullet_block_chance = 30

obj/structure/interactive/misc/resident/red_bochka
	name = "Металлическая бочка"
	desc = "Красная металлическая бочка."
	desc_extended = "Сильно поржавевший бочок, окрашенный в красный цвет. Выглядит красиво и придаёт местности своей эстетики, но абсолютно бесполезен."
	icon_state = "red_bochka"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALKING
	bullet_block_chance = 30

obj/structure/interactive/misc/resident/shit_ok
	name = "Энерго-щиток"
	desc = "Щиток."
	desc_extended = "Энерго-щиток. Отвечает за свет в здании, однако вся проводка давно перегорела."
	icon_state = "shitok2"
	icon = 'icons/stalker/decor.dmi'
	plane = PLANE_WALL

obj/structure/interactive/misc/resident/barrikada1
	name = "Ржавый металлический барьер"
	desc = "Достаточно старая металлическая баррикада."
	desc_extended = "Достаточно старая металлическая баррикада, которую кто-то пытался безуспешно восстановить, наклеивая поверх пластины от разного хлама."
	icon_state = "metal_barrier1"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 100

obj/structure/interactive/misc/resident/barrikada2
	name = "Металлический барьер"
	desc = "Металлическая баррикада."
	desc_extended = "Металлическая баррикада, которая когда-то использовалась в качестве укрытий от летающих молотовов и прочих не особо желанных предметов."
	icon_state = "metal_barrier3"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/brokent1
	name = "Перевёрнутый стол"
	desc = "Просто стол поставленный на бок."
	desc_extended = "Просто стол поставленный на бок. Можно использовать как небольшое укрытие, или, к примеру...Зачем ещё можно использовать перевёрнутый стол?"
	icon_state = "broke_table1"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALKING
	bullet_block_chance = 35

obj/structure/interactive/misc/resident/brokent2
	name = "Перевёрнутый стол"
	desc = "Просто стол поставленный на бок."
	desc_extended = "Просто стол поставленный на бок. Можно использовать как небольшое укрытие, или, к примеру...Зачем ещё можно использовать перевёрнутый стол?"
	icon_state = "broke_table2"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALKING
	bullet_block_chance = 35

obj/structure/interactive/misc/resident/trup1
	name = "Труп"
	desc = "Сгнивший труп"
	desc_extended = "Сгнивший труп, на котором виднеются многочисленные пулевые ранения. Он лежит тут явно больше пары недель."
	icon_state = "dead1"
	plane = PLANE_WALL

obj/structure/interactive/misc/resident/trup2
	name = "Труп"
	desc = "Сгнивший труп"
	desc_extended = "Сгнивший труп, буквально разорванный на части. Кто-то откусил или оторвал всё что было ниже его пояса. Он явно лежит тут достаточно давно."
	icon_state = "dead2"
	plane = PLANE_WALL

obj/structure/interactive/misc/resident/porog
	name = "Порог"
	desc = "Что ещё тебе нужно знать?."
	desc_extended = "Заниженный порог вхождения. Специально для твоего айкью."
	icon_state = "porog2"
	plane = -3

obj/structure/interactive/misc/resident/porog2
	name = "Порог"
	desc = "Что ещё тебе нужно знать?."
	desc_extended = "Заниженный порог вхождения. Специально для твоего айкью."
	icon_state = "porog1"
	plane = -12

obj/structure/interactive/misc/resident/bricks
	name = "Кучка кирпичей"
	desc = "Такими можно с лёгкостью разбить кому-то черепную коробку."
	desc_extended = "Стопка замшелых кирпичей, видимо, свалившихся откуда-то."
	icon_state = "bricks"
	plane = PLANE_WALL
	bullet_block_chance = 0

obj/structure/interactive/misc/resident/tv
	name = "Телевизор"
	desc = "Старый телевизор."
	desc_extended = "Старый, изрядно потрёпанный временем зомбо-ящик."
	icon_state = "TV"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 30

obj/structure/interactive/misc/resident/polka
	name = "Стеллаж"
	desc = "Старый стеллаж."
	desc_extended = "Старый, уже отчасти прогнивший стеллаж, используемый для складирования разного барахла."
	icon_state = "polka1"
	icon = 'icons/stalker/polka.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 20

/*obj/structure/interactive/misc/resident/polka/PostInitialize() //Random shelf.
	. = ..()
	icon_state = "polka[rand(1,9)]"
	return .*/

obj/structure/interactive/misc/resident/vertic
	name = "Вертолёт"
	desc = "Разбившийся вдребезги вертолёт."
	desc_extended = "Огромная металлическая птица, МИ-24. Когда-то летала высоко в небе, а сейчас - только и напоминает о своём былом величии, от которого остались лишь обломки да ржавчина."
	icon_state = "3,2"
	icon = 'icons/stalker/cars/mi-24.dmi'
	plane = -5
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 100

obj/structure/interactive/misc/resident/radio
	name = "Радио"
	desc = "Ржавое старое радио."
	desc_extended = "Коробка с торчащей из верхней части антенной. Пережиток прошлого, который больше никогда не заработает."
	icon_state = "radio"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 20

obj/structure/interactive/misc/resident/musorr1
	name = "Мусорный ящик"
	desc = "Пустой мусорный ящик."
	desc_extended = "Природа настолько изменилась, что мусорный ящик - стал частью местного декора."
	icon_state = "yashik_musor"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 35

obj/structure/interactive/misc/resident/musorr2
	name = "Мусорный ящик"
	desc = "Пустой мусорный ящик."
	desc_extended = "Природа настолько изменилась, что мусорный ящик - стал частью местного декора."
	icon_state = "yashik_musor_full"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/musorg1
	name = "Мусорный ящик"
	desc = "Пустой мусорный ящик."
	desc_extended = "Природа настолько изменилась, что мусорный ящик - стал частью местного декора."
	icon_state = "yashik_musor2"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 35

obj/structure/interactive/misc/resident/musorg2
	name = "Мусорный ящик"
	desc = "Пустой мусорный ящик."
	desc_extended = "Природа настолько изменилась, что мусорный ящик - стал частью местного декора."
	icon_state = "yashik_musor2_full"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/btr
	name = "БТР"
	desc = "Ржавый БТР."
	desc_extended = "Таким, когда-то, давили людей и нелюдей. Сейчас же - толк от него разве что как от укрытия есть."
	icon_state = "2"
	icon = 'icons/stalker/cars/btr2.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 100
	opacity = 1

obj/structure/interactive/misc/resident/car
	name = "Москвич"
	desc = "Старый Москвич."
	desc_extended = "На таком точно разъезжал кто-нибудь из твоих старших родственников. Земля им, к слову, пухом. Достаточно дырявый, местами корпуса вообще не осталось. Однако его всё ещё можно использовать как более-менее неплохое укрытие."
	icon_state = "2"
	icon = 'icons/stalker/cars/moskvich1_south.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/car2
	name = "УАЗ"
	desc = "Бортовой военный грузовик."
	desc_extended = "Когда-то использовался в качестве транспорта для военных и эвакуации гражданских. Сейчас же - толк от него разве что как от укрытия. И то, крайне сомнительного."
	icon_state = "2"
	icon = 'icons/stalker/cars/gruzovik_army_south.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	opacity = 1
	bullet_block_chance = 80

obj/structure/interactive/misc/resident/car4
	name = "УАЗ"
	desc = "Бортовой военный грузовик."
	desc_extended = "Когда-то использовался в качестве транспорта для военных и эвакуации гражданских. Сейчас же - толк от него разве что как от укрытия. И то, крайне сомнительного."
	icon_state = "2"
	icon = 'icons/stalker/cars/gruzovik_dead/north.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	opacity = 1
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/car2/north
	name = "УАЗ"
	desc = "Бортовой военный грузовик."
	desc_extended = "Когда-то использовался в качестве транспорта для военных и эвакуации гражданских. Сейчас же - толк от него разве что как от укрытия. И то, крайне сомнительного."
	icon_state = "2"
	icon = 'icons/stalker/cars/gruzovik_army_north.dmi'
	plane = PLANE_WALL
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 80

obj/structure/interactive/misc/resident/car3
	name = "Грузовик"
	desc = "Ржавый грузан."
	desc_extended = "На таких доставляли продовольствие и вещи на рынки/убежища. Вы до сих пор помните как люди топтали друг друга, пытаясь забрать с него побольше."
	icon_state = "2"
	icon = 'icons/stalker/cars/gruzovik_west.dmi'
	plane = PLANE_WALL
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 80

obj/structure/interactive/misc/resident/car3/east
	name = "Грузовик"
	desc = "Ржавый грузан."
	desc_extended = "На таких доставляли продовольствие и вещи на рынки/убежища. Вы до сих пор помните как люди топтали друг друга, пытаясь забрать с него побольше."
	icon_state = "2"
	icon = 'icons/stalker/cars/gruzovik_east.dmi'
	plane = PLANE_WALL
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 80

obj/structure/interactive/misc/resident/car3/north
	name = "Грузовик"
	desc = "Ржавый грузан."
	desc_extended = "На таких доставляли продовольствие и вещи на рынки/убежища. Вы до сих пор помните как люди топтали друг друга, пытаясь забрать с него побольше."
	icon_state = "2"
	icon = 'icons/stalker/cars/gruzovik_north.dmi'
	plane = PLANE_WALL
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 80

obj/structure/interactive/misc/resident/car3/south
	name = "Грузовик"
	desc = "Ржавый грузан."
	desc_extended = "На таких доставляли продовольствие и вещи на рынки/убежища. Вы до сих пор помните как люди топтали друг друга, пытаясь забрать с него побольше."
	icon_state = "2"
	icon = 'icons/stalker/cars/gruzovik_south.dmi'
	plane = PLANE_WALL
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 80

obj/structure/interactive/misc/resident/car5
	name = "УАЗ-469"
	desc = "Старый УАЗик."
	desc_extended = "На таком точно разъезжал кто-нибудь из твоих старших родственников. Земля им, к слову, пухом. Достаточно дырявый, местами корпуса вообще не осталось. Однако его всё ещё можно использовать как более-менее неплохое укрытие."
	icon_state = "0,2"
	icon = 'icons/stalker/cars/uaz-469_south.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/stolb
	name = "Столб"
	desc = "Просто столб."
	desc_extended = "Такой даже в качестве укрытия не сойдёт."
	icon_state = "stolb"
	icon = 'icons/stalker/decorations_32x64.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 10

obj/structure/interactive/misc/resident/sign/russian
	name = "Плакат"
	desc = "Старый рабочий плакат."
	desc_extended = "Старый плакат, используемый для пометки служебных помещений."
	icon_state = "RU_staff_only"
	icon = 'icons/stalker/some_stuff/decals.dmi'
	plane = PLANE_WALL

obj/structure/interactive/misc/resident/sign/russian2
	name = "Плакат"
	desc = "Старый рабочий плакат."
	desc_extended = "Увидя подобную надпись, любой другой нормальный человек прошёл бы мимо. Как жаль, что мы ненормальные."
	icon_state = "RU_electrical_danger"
	icon = 'icons/stalker/some_stuff/decals.dmi'
	plane = PLANE_WALL

obj/structure/interactive/misc/resident/sign/anime
	name = "Плакат"
	desc = "Старый плакат."
	desc_extended = "Старый плакат с предположительно японскими/китайскими иероглифами - в любом случае, разобрать ты их не можешь. Зато вот фразу ОПАСНО - вполне."
	icon_state = "CH_danger"
	icon = 'icons/stalker/some_stuff/decals.dmi'
	plane = PLANE_WALL

obj/structure/interactive/misc/resident/ostanki_dosok
	name = "Доски"
	desc = "Просто доски."
	desc_extended = "Чего ещё ты тут ожидаешь увидеть?"
	icon_state = "doski_oblomk1"
	plane = PLANE_WALL

obj/structure/interactive/misc/resident/ostanki_dosok/PostInitialize() //Random shelf.
	. = ..()
	icon_state = "doski_oblomk[rand(1,4)]"
	return .

obj/structure/interactive/misc/resident/doski
	name = "Заколоченный проход"
	desc = "Импровизированная баррикада из досок."
	desc_extended = "Одно из одновременно самых непрактичных и в то же время надёжных средств защиты от окружающего мира. Плотно прибитые доски на вряд ли дадутся голым рукам."
	icon_state = "zabitiy_proxod2"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/doski/clicked_on_by_object(mob/living/advanced)
	return

obj/structure/interactive/misc/resident/doski/clicked_on_by_object(obj/item/weapon/W, mob/living/advanced, params)
	if(istype(W, /obj/item/weapon/melee/resident/crowbar))
		play('sound/effects/crate_break.ogg',get_turf(src))
		qdel(src)
		new /obj/structure/interactive/misc/resident/ostanki_dosok(src.loc)
	else
		return clicked_on_by_object(advanced)

obj/structure/interactive/misc/resident/electric
	name = "Трансформатор"
	desc = "Старый, еле-работающий трансформатор."
	desc_extended = "Всё ещё вырабатывает энергию. Не смотря на внешний вид - внутри относительно неплохо сохранился."
	icon = 'icons/stalker/metro-2/decor3.dmi'
	icon_state = "transformator"
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/electricshield
	name = "Электро-щиток"
	desc = "Настенный электро-щиток."
	desc_extended = "Через эту штуку, когда-то, контролировалась подача питания на те или иные части здания. Интересно, а работает ли она до сих пор?..."
	icon = 'icons/stalker/metro-2/shieldwall.dmi'
	icon_state = "wallshield1"

obj/structure/interactive/misc/resident/electricshield/PostInitialize() //Random shit_ok
	. = ..()
	icon_state = "wallshield[rand(1,10)]"
	return .

obj/structure/interactive/misc/resident/tombstone
	name = "Надгробие"
	desc = "Дряхлое надгробие."
	desc_extended = "Старое мраморное надгробие. Кажется, оно некогда принадлежало кому-то. Интересно, он всё ещё там лежит?"
	icon_state = "tombstone"
	icon = 'icons/stalker/some_stuff/decals.dmi'

obj/structure/interactive/misc/resident/cross
	name = "Крест"
	desc = "Дряхлый крест."
	desc_extended = "Старый деревянный крест. Кажется, на нём виднеются какие-то инициалы. Интересно, он всё ещё лежит под ним?"
	icon_state = "cross"

obj/structure/interactive/misc/resident/grave
	name = "Могила"
	desc = "Закопанная могила."
	desc_extended = "Удивительно, что Тёмные не появились из них в своё время."
	icon_state = "grave_3"

obj/structure/interactive/misc/resident/cross2
	name = "Крест"
	desc = "Дряхлый крест."
	desc_extended = "Старый деревянный крест. Кажется, на нём виднеются какие-то инициалы. Интересно, он всё ещё лежит под ним?"
	icon_state = "wood_cross"

obj/structure/interactive/misc/resident/tombstone2
	name = "Надгробие"
	desc = "Дряхлое надгробие."
	desc_extended = "Старое мраморное надгробие. Кажется, оно некогда принадлежало кому-то. Интересно, он всё ещё там лежит?"
	icon_state = "grave1"
	icon = 'icons/stalker/some_stuff/decor.dmi'
	plane = PLANE_WALL
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/shkaff
	name = "Шкаф"
	desc = "Большой деревянный шкаф."
	desc_extended = "Огромный по своим размерам деревянный шкаф, способный вместить в себя до двух человек минимум. Жаль только, что абсолютно пустой."
	icon_state = "f_closet_1"
	icon = 'icons/structures.dmi'
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/shkaff2
	name = "Шкаф"
	desc = "Большой деревянный шкаф."
	desc_extended = "Огромный по своим размерам деревянный шкаф, способный вместить в себя до двух человек минимум. Жаль только, что абсолютно пустой."
	icon_state = "f_closet2"
	icon = 'icons/structures.dmi'
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/shkaff3
	name = "Шкаф"
	desc = "Большой деревянный шкаф."
	desc_extended = "Огромный по своим размерам деревянный шкаф, способный вместить в себя до двух человек минимум. Жаль только, что абсолютно пустой."
	icon_state = "f_closet_3"
	icon = 'icons/structures.dmi'
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 50

obj/structure/interactive/misc/resident/curtain_open2
	name = "curtain"
	desc = "For warding off peeping toms."
	desc_extended = "A curtain attached to the wall/ceiling as decor to liven up the area."
	icon_state = "curtain"
	icon = 'icons/stalker/lohweb/structures.dmi'

obj/structure/interactive/misc/resident/clocks
	name = "Часы"
	desc = "Деревянные башенные часы."
	desc_extended = "Достаточно старые часы. Основа давно прогнила, как, в прочем, заржавели и механизмы. Однако, что удивительно, они до сих пор работают."
	icon_state = "pclock"
	icon = 'icons/stalker/lohweb/miscobjs.dmi'

obj/structure/interactive/misc/resident/pod
	name = "Капсула"
	desc = "Какая-то капсула...Изнутри вытекает что-то чёрное."
	desc_extended = "От одного взгляда на эту хрень пробирает дрожью...А когда-то они валились с неба десятками."
	icon_state = "cultpod_cl"
	icon = 'icons/obj/structure/supply_pods.dmi'

obj/structure/interactive/misc/resident/pod2
	name = "Капсула"
	desc = "Какая-то капсула...Изнутри вытекает что-то чёрное."
	desc_extended = "От одного взгляда на эту хрень пробирает дрожью...А когда-то они валились с неба десятками."
	icon_state = "cultpod_op"
	icon = 'icons/obj/structure/supply_pods.dmi'

obj/structure/interactive/misc/resident/ladder
	name = "Лестница"
	desc = "Обычная металлическая лестница."
	desc_extended = "Чего ещё ты тут ждёшь?"
	icon_state = "ladder"
	icon = 'icons/structures.dmi'

obj/structure/interactive/misc/resident/ladder/up
	name = "Лестница"
	desc = "Обычная металлическая лестница."
	desc_extended = "Чего ещё ты тут ждёшь?"
	icon_state = "ladder_up"
	icon = 'icons/structures.dmi'