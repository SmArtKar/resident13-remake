/obj/item/key
	name = "key"
	desc = "Used to unlock things."
	icon = 'icons/items.dmi'
	icon_state = "keys"
	var/access = list()

/obj/item/key/New()
	..()
	icon_state = "key[rand(1, 4)]"


/obj/item/key/base
	name = "key"
	desc = "Used to unlock things. With label << Base >>, on the other side <<Don't forget to lock the Doors>>"
	access = list(BASE)

/obj/item/key/warehouse
	name = "key"
	desc = "Used to unlock things. With label << Warehouse >> and ont the other side of it <<I'll fuck ya if u...steal something>>"
	access = list(WAREHOUSE)

/obj/item/key/hydro
	name = "key"
	desc = "Used to unlock things. With label << Hydro >>"
	access = list(HYDRO)

/obj/item/key/bar
	name = "key"
	desc = "Used to unlock things. With label << Bar >>"
	access = list(BAR)

/obj/item/key/civil
	name = "key"
	desc = "Used to unlock things. With label << Civilian >> on the other side of it reminder: <<Locking toilets is probably bad idea>>"
	access = list(CIVIL)

/obj/item/key/head
	name = "key"
	desc = "Used to unlock things. With label << General >>, with tiny words: <<Congrats, now you're a head of that base>> "
	access = list(BASE, WAREHOUSE, HYDRO, BAR, CIVIL)

/obj/item/key/base/outlands_1
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_1)

/obj/item/key/base/outlands_2
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_2)

/obj/item/key/base/outlands_3
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_3)