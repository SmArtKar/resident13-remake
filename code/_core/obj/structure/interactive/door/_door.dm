#define DOOR_STATE_OPENED "opened"
#define DOOR_STATE_OPENING_01 "opening_01"
#define DOOR_STATE_OPENING_02 "opening_02"
#define DOOR_STATE_CLOSED "closed"
#define DOOR_STATE_CLOSING_01 "closing_01"
#define DOOR_STATE_CLOSING_02 "closing_02"
#define DOOR_STATE_LOCKED "locked" //Only used for singleplayer airlocks
#define DOOR_STATE_START_OPENING "start_opening" //Only used for airlocks
#define DOOR_STATE_DENY "denay" //Only used for airlocks
#define BASE 1
#define HYDRO 2
#define BAR 3
#define CIVIL 4
#define WAREHOUSE 5
#define OUTLANDS_1 11
#define OUTLANDS_2 12
#define OUTLANDS_3 13


obj/structure/interactive/door
	name = "door"
	desc = "What's on the other side?"
	collision_flags = FLAG_COLLISION_WALL
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	opacity = 1
	icon = 'icons/obj/structure/airlock/doors.dmi'
	icon_state = "woodrustic"

	var/door_state = DOOR_STATE_CLOSED

	var/open_time = 8
	var/close_time = 8

	var/locked = FALSE
	var/allow_manual_open = TRUE
	var/allow_manual_close = TRUE
	var/req_access = 0

	var/open_sound = null
	var/close_sound = null
	var/deny_sound = null

	var/spawn_signaller = FALSE
	var/radio_frequency = RADIO_FREQ_DOOR
	var/radio_signal = 20

	layer = LAYER_OBJ_DOOR_CLOSED

	plane = PLANE_WALL

/obj/structure/interactive/door/New(var/desired_loc)

	if(spawn_signaller)
		var/obj/item/device/signaller/S = new(src)
		S.name = "[name] [S.name]"
		S.frequency_current = radio_frequency
		S.signal_current = radio_signal
		INITIALIZE(S)
		GENERATE(S)
		door_state = DOOR_STATE_CLOSED
		locked = TRUE

	return ..()

/obj/structure/interactive/door/PostInitialize()
	. = ..()
	update_sprite()
	return .

obj/structure/interactive/door/update_icon()
	..()
	switch(door_state)
		if(DOOR_STATE_OPENING_01)
			icon_state = "[initial(icon_state)]opening"
			desc = "The door is opening."
			update_collisions(FLAG_COLLISION_WALL,FLAG_COLLISION_BULLET_INORGANIC)
			layer = LAYER_OBJ_DOOR_CLOSED
			set_opacity(0)

		if(DOOR_STATE_CLOSING_01)
			icon_state = "[initial(icon_state)]closing"
			desc = "The door is closing."
			update_collisions(FLAG_COLLISION_NONE,FLAG_COLLISION_BULLET_NONE)
			layer = LAYER_OBJ_DOOR_CLOSED
			set_opacity(0)

		if(DOOR_STATE_OPENED)
			icon_state = "[initial(icon_state)]open"
			desc = "The door is open."
			update_collisions(FLAG_COLLISION_NONE,FLAG_COLLISION_BULLET_NONE)
			layer = LAYER_OBJ_DOOR_OPEN
			set_opacity(0)

		if(DOOR_STATE_CLOSED)
			icon_state = initial(icon_state)
			desc = "The door is closed."
			update_collisions(FLAG_COLLISION_WALL,FLAG_COLLISION_BULLET_INORGANIC)
			layer = LAYER_OBJ_DOOR_CLOSED
			set_opacity(initial(opacity))

obj/structure/interactive/door/proc/toggle(var/atom/caller,var/lock = FALSE,var/force = FALSE)
	if(door_state == DOOR_STATE_OPENED)
		close(caller)
		return TRUE
	else if(door_state == DOOR_STATE_CLOSED)
		open(caller)
		return TRUE
	return FALSE

obj/structure/interactive/door/proc/open(var/atom/caller,var/lock = FALSE,var/force = FALSE)
	if(open_sound)
		play(open_sound,src)
		if(caller) create_alert(VIEW_RANGE,src,caller,ALERT_LEVEL_NOISE)
	door_state = DOOR_STATE_OPENING_01
	update_sprite()
	spawn(open_time)
		door_state = DOOR_STATE_OPENED
		update_sprite()


obj/structure/interactive/door/proc/close(var/atom/caller,var/lock = FALSE,var/force = FALSE)
	if(close_sound)
		play(close_sound,src)
		if(caller) create_alert(VIEW_RANGE,src,caller,ALERT_LEVEL_NOISE)
	door_state = DOOR_STATE_CLOSING_01
	update_sprite()
	spawn(close_time)
		door_state = DOOR_STATE_CLOSED
		update_sprite()

/obj/structure/interactive/door/proc/unlock(var/atom/caller,var/force = FALSE)
	locked = FALSE
	update_sprite()
	return TRUE

/obj/structure/interactive/door/proc/lock(var/atom/caller,var/force = FALSE)
	locked = TRUE
	update_sprite()
	return TRUE

obj/structure/interactive/door/clicked_on_by_object(var/mob/caller,object,location,control,params)

	INTERACT_CHECK

	var/atom/A = check_interactables(caller,object,location,control,params)
	if(A && A.clicked_on_by_object(caller,object,location,control,params))
		return TRUE

	//var/obj/item/I = check_interactables(caller,object,location,control,params)
	//var/mob/living/advanced/player/P = caller
	//var/obj/item/key/K = object
	if(istype(object, /obj/item/key))//I, /obj/item/key/K // && door_state == DOOR_STATE_CLOSED
		var/obj/item/key/K = object
		var/L[] = K.access
		var/allowed = 0
		if(!req_access) //Если у нас ключ вовсе оказался не ключом
			allowed = 1
		if(!islist(req_access)) //Если дверь не требует доступа
			allowed = 1
		if(!islist(L)) //Если ключ без доступа
			caller.to_chat("Этот ключ сломан...")
			return
		var/i
		for(i in req_access)
			if(!(i in L)) //Неподходящий ключ
				caller.to_chat("Этот ключ не подходит")
				return //Если нет доступа
			if(i in L) //Нужный нам ключ
				allowed = 1

		allowed = 1
		if(allowed == 1)
			if(locked == FALSE)
				lock(caller)
				caller.to_chat("Закрыто")
			else if(locked == TRUE)
				unlock(caller)
				caller.to_chat("Открыто")
			allowed = 0
	else if(door_state == DOOR_STATE_OPENED && allow_manual_close && locked == FALSE)
		close(caller)
	else if(door_state == DOOR_STATE_CLOSED && allow_manual_open && locked == FALSE)
		open(caller)
	else if(locked == TRUE)
		caller.to_chat("Дверь заперта")
	return TRUE

obj/structure/interactive/door/wood
	name = "wooden door"

obj/structure/interactive/door/metal
	name = "iron door"
	icon_state = "silver"
	color = "#888888"

/*obj/structure/interactive/door/resident/clicked_on_by_object(obj/item/I, mob/living/advanced, params)
	if(istype(I, /obj/item/key) && lock)
		var/obj/item/key/K = I
		if(!lock.toggle(I))
			advanced.to_chat(advanced, "<span class='warning'>\The [K] does not fit in the lock!</span>")
		else
			advanced.to_chat(advanced, "<span class='warning'>You unlock the door.</span>")
		return

	if(lock && lock.isLocked())
		advanced.to_chat(advanced, "\The [src] is locked!")

	return

obj/structure/interactive/door/resident/click_on_object(mob/living/advanced as mob)
	if(lock && lock.isLocked())
		advanced.to_chat(advanced, "\The [src] is locked!")
		return

/obj/machinery/door/unpowered/simple/can_open()
	if(!..() || (lock && lock.isLocked()))
		return 0
	return 1*/

obj/structure/interactive/door/resident/metal
	name = "metal door"
	icon_state = "metal2"
	icon = 'icons/doors.dmi'
	opacity = 0
	bullet_block_chance = 65

obj/structure/interactive/door/resident/metal/base
	req_access = list(BASE)
	locked = TRUE

obj/structure/interactive/door/resident/metal/hydro
	req_access = list(HYDRO)

obj/structure/interactive/door/resident/metal/bar
	req_access = list(BAR)

obj/structure/interactive/door/resident/metal/warehouse
	req_access = list(WAREHOUSE)

obj/structure/interactive/door/resident/metal/civil
	req_access = list(CIVIL)

obj/structure/interactive/door/resident/wood2/outlands_1
	req_access = list(OUTLANDS_1)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_2
	req_access = list(OUTLANDS_2)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_3
	req_access = list(OUTLANDS_3)
	locked = TRUE


obj/structure/interactive/door/resident/wood
	name = "wood door"
	icon_state = "wood"
	icon = 'icons/doors.dmi'
	opacity = 1
	bullet_block_chance = 50

obj/structure/interactive/door/resident/wood2
	name = "wood door"
	icon_state = "old"
	icon = 'icons/doors.dmi'
	opacity = 1
	bullet_block_chance = 30

obj/structure/interactive/door/resident/fancy
	name = "wood door"
	icon_state = "fancy"
	icon = 'icons/doors.dmi'
	opacity = 1
	bullet_block_chance = 30

obj/structure/interactive/door/resident/fancy2
	name = "wood door"
	icon_state = "fancy2"
	icon = 'icons/doors.dmi'
	opacity = 0
	bullet_block_chance = 10