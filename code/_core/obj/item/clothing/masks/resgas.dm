/obj/item/clothing/mask/gas/res/destro_op
	name = "advanced gas mask"
	icon = 'obj/item/clothing/masks/gasre2.dmi'
	flags_clothing = FLAG_CLOTHING_NOBEAST_HEAD
	desc = "Oxygen not included."
	desc_extended = "Значительно улучшенный противогаз, некогда выпускаемый Дестро для своих работников и силовиков."
	rarity = RARITY_RARE

	defense_rating = list(
		BLADE = 30,
		BLUNT = 30,
		PIERCE = 30,
		MAGIC = -50,
		HEAT = 60,
		BIO = 120,
		RAD = 90
	)

	size = SIZE_2
	weight = WEIGHT_3

	value = 80

/obj/item/clothing/mask/gas/res/civilian
	name = "civilian gas mask"
	icon = 'obj/item/clothing/masks/gasre1.dmi'
	flags_clothing = FLAG_CLOTHING_NOBEAST_HEAD
	desc = "Противогаз."
	desc_extended = "Обычный гражданский противогаз."
	rarity = RARITY_UNCOMMON

	defense_rating = list(
		BLADE = 15,
		BLUNT = 15,
		PIERCE = 15,
		MAGIC = -60,
		HEAT = 40,
		BIO = 90,
		RAD = 50
	)

	size = SIZE_2
	weight = WEIGHT_3

	value = 40