/obj/item/clothing/uniform/res/destro_op
	name = "военная униформа"
	desc = "Такую раньше носило ЧВК Дестро"
	desc_extended = "Удобная, практичная, не сковывающая движение. Что ещё нужно для счастья?"
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/destro_op.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 15,
		BLUNT = 10,
		PIERCE = 15,
		LASER = -50,
		MAGIC = -50,
		COLD = 30
	)

	value = 60

/obj/item/clothing/uniform/res/destro_scientist
	name = "рубашка и джинсы"
	desc = "Просто рубашка и джинсы"
	desc_extended = "Немного помятая одежда, но зато практичная и не мешающая движению."
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/civ1.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 5,
		BLUNT = 5,
		PIERCE = 5,
		LASER = -50,
		MAGIC = -50,
		COLD = 15
	)

	value = 30

/obj/item/clothing/uniform/res/civ
	name = "Рубашка и джинсы"
	desc = "Просто рубашка и джинсы"
	desc_extended = "Немного помятая одежда, но зато практичная и не мешающая движению."
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/civ2.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 5,
		BLUNT = 5,
		PIERCE = 5,
		LASER = -50,
		MAGIC = -50,
		COLD = 15
	)

	value = 30

/obj/item/clothing/uniform/res/civ2
	name = "Рубашка и штаны"
	desc = "Просто рубашка и чёрные утеплённые штаны"
	desc_extended = "Немного помятая одежда, но зато практичная и не мешающая движению."
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/civ3.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 5,
		BLUNT = 5,
		PIERCE = 5,
		LASER = -50,
		MAGIC = -50,
		COLD = 30
	)

	value = 30

/obj/item/clothing/uniform/res/civ3
	name = "Кофта и штаны"
	desc = "Просто рубашка и коричневые штаны"
	desc_extended = "Немного помятая одежда, но зато практичная и не мешающая движению."
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/civ4.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 5,
		BLUNT = 5,
		PIERCE = 5,
		LASER = -50,
		MAGIC = -50,
		COLD = 15
	)

	value = 30

/obj/item/clothing/uniform/res/bandosi
	name = "Кофта с символикой и штаны"
	desc = "Такое обычно носят плохие парни"
	desc_extended = "Утеплённые кофта и серые штаны, с символикой в виде черепа на спине."
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/bandosi.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 10,
		BLUNT = 10,
		PIERCE = 10,
		LASER = -50,
		MAGIC = -50,
		COLD = 40
	)

	value = 30

/obj/item/clothing/uniform/res/bandosi2
	name = "Спортивки"
	desc = "Чёрный спортивный костюм с двумя полосками"
	desc_extended = "Немного помятая одежда, но зато практичная и не мешающая движению."
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/bandosi2.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 5,
		BLUNT = 5,
		PIERCE = 5,
		LASER = -50,
		MAGIC = -50,
		COLD = 15
	)

	value = 30

/obj/item/clothing/uniform/res/donor
	name = "Костюм механика"
	desc = "Потёртый комбенизон"
	desc_extended = "Местами обляпанный непонятно чем комбенизон механика."
	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET
	icon = 'icons/obj/item/clothing/uniforms/donorcov.dmi'

	item_slot = SLOT_TORSO | SLOT_GROIN

	flags_clothing = FLAG_CLOTHING_NOBEAST_FEET

	defense_rating = list(
		BLADE = 5,
		BLUNT = 5,
		PIERCE = 5,
		LASER = -50,
		MAGIC = -50,
		COLD = 15
	)

	value = 30