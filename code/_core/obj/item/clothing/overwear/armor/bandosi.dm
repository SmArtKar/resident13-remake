/obj/item/clothing/overwear/armor/res/customvest
	name = "\improper Самодельный бронежилет"
	icon = 'icons/obj/item/clothing/suit/custom_vest.dmi'
	desc = "Лёгкий самопал."
	desc_extended = "Лёгкий самопальный бронежилет, буквально собранный из говна и палок. Защищает как сука, но это лучшее, что можно в принципе найти."

	protected_limbs = list(BODY_TORSO,BODY_GROIN)

	rarity = RARITY_UNCOMMON

	defense_rating = list(
		BLADE = 15,
		BLUNT = 45,
		PIERCE = 20,
		LASER = 0,
		MAGIC = -100
	)

	size = SIZE_2
	weight = WEIGHT_2

	value = 50