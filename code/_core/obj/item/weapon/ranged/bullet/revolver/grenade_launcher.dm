/obj/item/weapon/ranged/bullet/revolver/grenade_launcher
	name = "M79 Thumper"
	desc = "Blooper"
	desc_extended = "an old grenade launcher from a past era, uses 40mm grenades."
	icon = 'icons/obj/item/weapons/ranged/grenade_launcher.dmi'
	icon_state = "inventory"

	shoot_delay = 1.25

	automatic = TRUE

	bullet_count_max = 1

	insert_limit = 1

	view_punch = TILE_SIZE - 1

	shoot_sounds = list('sound/weapons/grenade_launcher/thump.ogg')

	slowdown_mul_held = HELD_SLOWDOWN_SHOTGUN

	size = SIZE_4

	bullet_length_min = 45
	bullet_length_best = 46
	bullet_length_max = 47

	bullet_diameter_min = 39
	bullet_diameter_best = 40
	bullet_diameter_max = 41

	heat_per_shot = 0.09
	heat_max = 0.18

	value = 20

	open = TRUE

/obj/item/weapon/ranged/bullet/revolver/grenade_launcher/get_base_spread()
	return 0.05

/obj/item/weapon/ranged/bullet/revolver/grenade_launcher/get_static_spread()
	return 0.01

/obj/item/weapon/ranged/bullet/revolver/grenade_launcher/get_skill_spread(var/mob/living/L)
	return max(0,0.03 - (0.12 * L.get_skill_power(SKILL_RANGED)))