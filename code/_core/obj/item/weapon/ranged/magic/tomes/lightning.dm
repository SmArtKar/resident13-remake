/obj/item/weapon/ranged/magic/tome/lightning
	name = "strange device"
	desc = "Be like Zeus!"
	desc_extended = "Fires slow moving lightning bolts at foes."
	cost_mana = 50
	shoot_delay = 10

	item_slot = SLOT_HAND_RIGHT | SLOT_HAND_LEFT
	worn_layer = LAYER_MOB_CLOTHING_BACK
	slot_icons = TRUE

	icon = 'icons/obj/item/weapons/ranged/magic/tomes/lightning.dmi'

	projectile = /obj/projectile/magic/lightning_bolt

	ranged_damage_type = /damagetype/ranged/magic/lightning

	shoot_sounds = list('sound/weapons/magic/zap_large.ogg')

	projectile_speed = TILE_SIZE - 1

	value = 300

/obj/item/weapon/ranged/magic/tome/lightning/can_be_worn(var/mob/living/advanced/owner,var/obj/hud/inventory/I)
	return TRUE