/obj/item/weapon/ranged/magic/tome/chaos
	name = "strange device"
	desc = "Can't aim? Shoot balls of pure chaos around you."
	desc_extended = "Costs mana to use."
	cost_mana = 50
	shoot_delay = 10

	item_slot = SLOT_HAND_RIGHT | SLOT_HAND_LEFT
	worn_layer = LAYER_MOB_CLOTHING_BACK
	slot_icons = TRUE

	icon = 'icons/obj/item/weapons/ranged/magic/tomes/chaos.dmi'

	bullet_count = 9
	projectile_speed = TILE_SIZE - 1

	projectile = /obj/projectile/magic/chaos
	ranged_damage_type = /damagetype/ranged/magic/chaos

	shoot_sounds = list('sound/weapons/magic/chaos.ogg')

	value = 200

/obj/item/weapon/ranged/magic/tome/chaos/get_projectile_offset(var/initial_offset_x,var/initial_offset_y,var/bullet_num,var/accuracy)

	var/num = bullet_num/bullet_count

	var/norm_x = initial_offset_x + sin(num*360)
	var/norm_y = initial_offset_y + cos(num*360)

	var/mul = max(abs(norm_x),abs(norm_y))

	return list(norm_x/mul,norm_y/mul)

/obj/item/weapon/ranged/magic/tome/chaos/get_static_spread() //Base spread
	return 0

/obj/item/weapon/ranged/magic/tome/chaos/can_be_worn(var/mob/living/advanced/owner,var/obj/hud/inventory/I)
	return TRUE
