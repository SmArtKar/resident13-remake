/obj/item/weapon/melee/resident/crowbar
	name = "Лом"
	desc = "Против лома нет приёма"
	desc_extended = "Лучший помощник в взломе...И ломании черепов."
	icon = 'icons/obj/item/weapons/melee/resident/crowbar.dmi'
	damage_type = /damagetype/melee/club/crowbar/

	value = 20

/obj/item/weapon/melee/resident/golf
	name = "Клюшка для гольфа"
	desc = "Главный двигатель сюжета TLOU2!"
	desc_extended = "Когда-то, эта крошка использовалась для игр, пускай и не очень популярных по миру. Сейчас, в принципе, ничего не изменилось, только игры заменила борьба за выживание."
	icon = 'icons/obj/item/weapons/melee/resident/golf.dmi'
	damage_type = /damagetype/melee/club/golf/

	value = 15

	size = SIZE_5
	weight = WEIGHT_4

/obj/item/weapon/melee/resident/bone
	name = "Дубинка из костей"
	desc = "У-у-у, страшно!"
	desc_extended = "Для любителей оккультики."
	icon = 'icons/obj/item/weapons/melee/resident/bone.dmi'
	damage_type = /damagetype/melee/club/bone/

	value = 15

	size = SIZE_5
	weight = WEIGHT_4

/obj/item/weapon/melee/resident/spear_stles
	name = "металлическое копьё"
	desc = "Тупое. Как ты."
	desc_extended = "Отлично подойдёт, если нужно кого-нибудь отвлечь. Убить кого-то им - на вряд ли возможно."
	icon = 'icons/obj/item/weapons/melee/swords/spear_weak.dmi'
	damage_type = /damagetype/melee/spear_weak/
	damage_type_thrown = /damagetype/melee/spear/thrown_weak
	crafting_id = "spearstels"

	attack_delay = 5
	attack_delay_max = 12

	value = 10

/obj/item/weapon/melee/sword/resident/knife
	name = "самодельный нож"
	desc = "Удобен для разделывания добычи"
	desc_extended = "Лучший помощник в вскрытии туш поверженных тварей и диких животных...Кто о чём."
	icon = 'icons/obj/item/weapons/melee/swords/knife.dmi'
	damage_type = /damagetype/melee/sword/knife/
	crafting_id = "knoif"

	attack_delay = 5
	attack_delay_max = 10

	value = 10