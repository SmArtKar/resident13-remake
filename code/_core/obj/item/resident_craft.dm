/obj/item/resident/crafting
	name = "allo"
	desc = "алло"
	desc_extended = "до связи"
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "scrap2"

/obj/item/resident/crafting/metall
	name = "Металлолом"
	desc = "Куча бесполезных запчастей и сплавов."
	desc_extended = "Куча бесполезных запчастей и металлических сплавов. С этим мало что можно сделать по раздельности, однако для создания чего-либо - это крайне необходимый хлам."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "scrap2"
	crafting_id = "scrap"
	weight = 0.80

/obj/item/resident/crafting/lenta
	name = "Изолента"
	desc = "Лучший друг самоделкина."
	desc_extended = "Когда заходишь в тупик, не предствляя, как сделать своё оружие ещё лучше - просто используй изоленту. Всем известно, что изолента делает любое творение в сто раз лучше."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "ducttape"
	crafting_id = "dutch"
	weight = 0.05

/obj/item/resident/crafting/ruchka
	name = "Деревянная ручка"
	desc = "Хм?"
	desc_extended = "Основа для небольшого по размерам оружия."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "bladeruchka"
	crafting_id = "ruchka"
	weight = 0.10

/obj/item/resident/crafting/ruchka_big
	name = "Деревянная ручка"
	desc = "Хм?"
	desc_extended = "Если нужно набить кому-то морду, или сделать факел."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "stick"
	crafting_id = "ruchka_big"
	weight = 0.20

/obj/item/resident/crafting/cloth
	name = "Ткань"
	desc = "Хм?"
	desc_extended = "Обычная ткань."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "clothbandaid"
	crafting_id = "cloth"
	weight = 0.02

/obj/item/resident/crafting/blade
	name = "Лезвие"
	desc = "Металлическое лезвие"
	desc_extended = "Основа для небольшого по размерам оружия."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "knifeblade0"
	crafting_id = "blade"
	weight = 0.10

/obj/item/resident/crafting/wood
	name = "Замшелые доски"
	desc = "Когда нужно сделать основу для чего-то."
	desc_extended = "Используется для создания баррикад, рукоятий, и много чего ещё."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "sheet-wood"
	crafting_id = "wood"

	item_count_current = 1
	item_count_max = 30
	item_count_max_icon = 1
	weight = 0.30

/obj/item/resident/decor/soup
	name = "Кастрюля"
	desc = "В такой варят и греют еду. Удивительно?"
	desc_extended = "Сейчас бы каши..."
	icon = 'icons/obj/item/resident.dmi'
	icon_state = "soup1"
	anchored = TRUE

/obj/item/resident/decor/soup/PostInitialize() //Random shelf.
	. = ..()
	icon_state = "soup[rand(1,11)]"
	return .

/obj/item/resident/crafting/shard
	name = "shard"
	desc = "I am error."
	icon = 'icons/obj/item/material.dmi'
	icon_state = "shard"

	crafting_id = "shard"
	weight = 0.02

/obj/item/resident/crafting/rod
	name = "rod"
	desc = "I am error."
	icon = 'icons/obj/item/material.dmi'
	icon_state = "rod"
	color = "#474747"

	item_count_current = 1
	item_count_max = 30
	item_count_max_icon = 3

	weight = 0.25

	crafting_id = "rod"