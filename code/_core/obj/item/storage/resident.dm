/obj/item/storage/ifak
	name = "ИФАК"
	desc = "Компактная аптечка."
	desc_extended = "Идеально подойдёт для оказания первой помощи, но не более того."
	icon = 'icons/IFAK.dmi'
	icon_state = "ifak"

	dynamic_inventory_count = 4
	container_held_slots = 1
	value = 1

	container_whitelist = list(
		/obj/item/container/syringe/medipen/resident/stamina,
		/obj/item/container/syringe/medipen/resident/pain,
		/obj/item/container/medicine/bandage,
		/obj/item/container/spray/resident
	)

/obj/item/storage/ifak/fill_inventory()
	new /obj/item/container/syringe/medipen/resident/pain(src)
	new /obj/item/container/syringe/medipen/resident/stamina(src)
	new /obj/item/container/medicine/bandage(src)
	new /obj/item/container/spray/resident(src)
	return ..()

	size = SIZE_3
	weight = WEIGHT_1

/obj/item/storage/ifak/update_icon()

	. = ..()

	var/filled_slots = 0
	for(var/obj/hud/inventory/I in src.inventories)
		filled_slots += length(I.held_objects)

	icon_state = "[initial(icon_state)][clamp(filled_slots,1,4)]"

	return .

/obj/item/storage/ifak/update_inventory()
	. = ..()
	update_sprite()
	return .

/obj/item/storage/bandage
	name = "Упаковка бинтов"
	desc = "Коричневая упаковка, внутри лежат бинты."
	desc_extended = "Коричневая упаковка, вмещающая в себя два полных мотка бинтов."
	icon = 'icons/obj/item/storage/bandages.dmi'
	icon_state = "bandage"

	dynamic_inventory_count = 2
	container_held_slots = 1
	value = 1

	container_whitelist = list(
		/obj/item/container/medicine/bandage
	)

/obj/item/storage/bandage/fill_inventory()
	new /obj/item/container/medicine/bandage(src)
	new /obj/item/container/medicine/bandage(src)
	return ..()

	size = SIZE_3
	weight = WEIGHT_1

/obj/item/storage/bandage/update_icon()

	. = ..()

	var/filled_slots = 0
	for(var/obj/hud/inventory/I in src.inventories)
		filled_slots += length(I.held_objects)

	icon_state = "[initial(icon_state)][clamp(filled_slots,0,2)]"

	return .

/obj/item/storage/bandage/update_inventory()
	. = ..()
	update_sprite()
	return .

/obj/item/storage/glass
	name = "glass box"
	icon_state = "smallbox"

	dynamic_inventory_count = 6
	container_held_slots = 1

/obj/item/storage/glass/fill_inventory()
	new /obj/item/container/beaker/shot(src)
	new /obj/item/container/beaker/shot(src)
	new /obj/item/container/beaker/shot(src)
	new /obj/item/container/beaker/shot(src)
	new /obj/item/container/beaker/glass(src)
	new /obj/item/container/beaker/glass(src)
	return ..()

/obj/item/storage/irp
	name = "ИРП"
	icon_state = "box1"

	dynamic_inventory_count = 6
	container_held_slots = 1

/obj/item/storage/irp/fill_inventory()
	new /obj/item/container/food/package/junkfood/cake(src)
	new /obj/item/container/food/package/junkfood/raisins(src)
	new /obj/item/container/food/package/junkfood/cheese_chips(src)
	new /obj/item/container/food/package/junkfood/chips(src)
	new /obj/item/container/food/package/junkfood/syndicate(src)
	new /obj/item/container/food/package/junkfood/jerky(src)
	return ..()