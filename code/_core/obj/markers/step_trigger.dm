/* Simple object type, calls a proc when "stepped" on by something */

/obj/effect/step_trigger
	var/affect_ghosts = 0
	var/stopper = 1 // stops throwers
	var/mobs_only = 0
	invisibility = 101 // nope cant see this shit
	anchored = 1

/obj/effect/step_trigger/proc/Trigger(atom/movable/A)
	return 0

/obj/effect/step_trigger/Crossed(H as mob|obj)
	..()
	if(!H)
		return
	if(!istype(H, /mob) && mobs_only)
		return
	Trigger(H)

/* Sends a message to mob when triggered*/

/obj/effect/step_trigger/message
	var/message	//the message to give to the mob
	var/once = 1

/obj/effect/step_trigger/message/Crossed(mob/M)
	if(M.client)
		M.to_chat(span("danger","[message]"))
		if(once)
			qdel(src)

/* Stops things thrown by a thrower, doesn't do anything */

/obj/effect/step_trigger/stopper
	invisibility = 101

/* Instant teleporter */

/obj/effect/step_trigger/teleporter
	name = "transfer zone"
	invisibility = 101
	icon = 'icons/stalker/areas.dmi'
	icon_state = "transfer_zone"
	var/teleport_x = 0	// teleportation coordinates (if one is null, then no teleport!)
	var/teleport_y = 0
	var/teleport_z = 0

/obj/effect/step_trigger/teleporter/Trigger(atom/movable/A)
	if(teleport_x && teleport_y && teleport_z)

		A.x = teleport_x
		A.y = teleport_y
		A.z = teleport_z

/* Random teleporter, teleports atoms to locations ranging from teleport_x - teleport_x_offset, etc */

/obj/effect/step_trigger/teleporter/random
	var/teleport_x_offset = 0
	var/teleport_y_offset = 0
	var/teleport_z_offset = 0

/obj/effect/step_trigger/teleporter/random/Trigger(atom/movable/A)
	if(teleport_x && teleport_y && teleport_z)
		if(teleport_x_offset && teleport_y_offset && teleport_z_offset)

			A.x = rand(teleport_x, teleport_x_offset)
			A.y = rand(teleport_y, teleport_y_offset)
			A.z = rand(teleport_z, teleport_z_offset)

/* Simple sound player, Mapper friendly! */

/*/obj/effect/step_trigger/sound_effect
	var/sound //eg. path to the sound, inside '' eg: 'growl.ogg'
	var/volume = 100
	var/freq_vary = 1 //Should the frequency of the sound vary?
	var/extra_range = 0 // eg World.view = 7, extra_range = 1, 7+1 = 8, 8 turfs radius
	var/happens_once = 0
	var/triggerer_only = 0 //Whether the triggerer is the only person who hears this


/obj/effect/step_trigger/sound_effect/Trigger(atom/movable/A)
	var/turf/T = get_turf(A)

	if(!T)
		return

	if(triggerer_only)
		A.playsound_local(T, sound, volume, freq_vary)
	else
		playsound(T, sound, volume, freq_vary, extra_range)

	if(happens_once)
		qdel(src)*/

var/list/sidorRooms = list()
var/obj/sidor_enter/sidorEnter = null

/obj/sidor_enter
	var/roomtype = "sidor"
	invisibility = 101

/obj/sidor_enter/New()
	sidorEnter = src

/obj/sidor_enter/Crossed(atom/movable/A)
	SendToEmptyRoom(A)

/obj/sidor_enter/proc/SendToEmptyRoom(atom/movable/A)
	var/obj/sidor_exit/Room = GetEmptyRoom()
	if(Room)
		A.loc = Room.loc
		if(istype(A,/mob/living/advanced))
			Room.occupant = A

/obj/sidor_enter/proc/GetEmptyRoom()
	for(var/obj/sidor_exit/R in sidorRooms)
		if(R.roomtype != roomtype)
			continue
		if(!R.occupant)
			return R
	world << "<b>Матрица Сидоровичей дала сбой: Недостаточно комнат!</b>"
	return


/obj/sidor_exit
	var/roomtype = "sidor"
	var/mob/living/advanced/occupant = null
	invisibility = 101

/obj/sidor_exit/New()
	sidorRooms.Add(src)

/obj/sidor_exit/Crossed(atom/movable/A)
	A.loc = sidorEnter.loc
	if(istype(A,/mob/living/advanced))
		occupant = null
		A << sound(null)