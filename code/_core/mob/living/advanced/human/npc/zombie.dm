/mob/living/advanced/npc/zombie
	name = "zombie"
	ai = /ai/advanced/zombie
	class = /class/zombie/

	species = "zombie"


	var/loadout_to_use = /loadout/zombie

/mob/living/advanced/npc/zombie/New(loc,desired_client,desired_level_multiplier)
	setup_sex()
	return ..()

/mob/living/advanced/npc/zombie/proc/setup_sex()
	gender = pick(MALE,FEMALE)
	sex = gender //oh god oh fuck what have i done
	return TRUE

/mob/living/advanced/npc/zombie/Initialize()

	. = ..()

	update_all_blends()
	equip_loadout(loadout_to_use)

	return .

/*/mob/living/advanced/npc/zombie/get_emote_sound(var/emote_id)

	switch(emote_id)
		if("pain")
			return null

	return null*/


/*/mob/living/advanced/npc/zombie/on_life_slow()

	. = ..()

	if(. && ai && ai.active && next_talk <= world.time && prob(25))

		var/sound_to_play

		if(ai.alert_level == ALERT_LEVEL_NONE)
			var/list/valid_sounds = list(
				'sound/voice/zombie/generic_01.ogg',
				'sound/voice/zombie/generic_02.ogg',
				'sound/voice/zombie/generic_03.ogg',
				'sound/voice/zombie/generic_04.ogg'
			)
			sound_to_play = pick(valid_sounds)
		else
			var/list/valid_sounds = list(
				'sound/voice/zombie/alert_01.ogg',
				'sound/voice/zombie/alert_02.ogg',
				'sound/voice/zombie/alert_03.ogg',
				'sound/voice/zombie/alert_04.ogg',
				'sound/voice/zombie/alert_05.ogg'
			)
			sound_to_play = pick(valid_sounds)

		if(sound_to_play) play(sound_to_play,get_turf(src))

		next_talk = world.time + SECONDS_TO_DECISECONDS(rand(5,12))

	return .*/


/*/mob/living/advanced/npc/zombie/attack(var/atom/attacker,var/atom/victim,var/list/params,var/atom/blamed,var/ignore_distance = FALSE, var/precise = FALSE) //The src attacks the victim, with the blamed taking responsibility

	. = ..()

	if(. && next_talk <= world.time && prob(50))
		var/list/valid_sounds = list(
			'sound/voice/zombie/attack_01.ogg',
			'sound/voice/zombie/attack_02.ogg',
			'sound/voice/zombie/attack_03.ogg',
			'sound/voice/zombie/attack_04.ogg'
		)
		play(pick(valid_sounds),get_turf(src))
		next_talk = world.time + SECONDS_TO_DECISECONDS(rand(5,12))
	return .*/

/mob/living/advanced/npc/zombie/on_damage_received(var/atom/atom_damaged,var/atom/attacker,var/atom/weapon,var/list/damage_table,var/damage_amount,var/critical_hit_multiplier,var/stealthy=FALSE)

	. = ..()

	if(!stealthy && !dead && damage_amount > 20 && prob(50))
		var/list/valid_sounds = list(
			'sound/voice/zombie/pain_01.ogg',
			'sound/voice/zombie/pain_02.ogg',
			'sound/voice/zombie/pain_03.ogg',
			'sound/voice/zombie/pain_04.ogg',
			'sound/voice/zombie/pain_05.ogg',
			'sound/voice/zombie/pain_06.ogg'
		)
		play(pick(valid_sounds),get_turf(src))

	return .


/mob/living/advanced/npc/zombie/post_death()

	. = ..()

	if(prob(50))
		var/list/valid_sounds = list(
			'sound/voice/zombie/death_01.ogg'
		)
		play(pick(valid_sounds),get_turf(src))

	return .

/mob/living/advanced/npc/zombie/standart
	loadout_to_use = /loadout/zombie/standart
	level_multiplier = 1.5

/mob/living/advanced/npc/zombie/heavy
	loadout_to_use = /loadout/zombie/heavy
	level_multiplier = 1.5