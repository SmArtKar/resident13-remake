mob/living/advanced/player/proc/can_save(var/area/A)

	if(!client)
		return FALSE

	if(!A || !(A.flags_area & FLAGS_AREA_SAVEZONE))
		src.to_chat(span("danger","Тут слишком опасно, нужно найти место по-спокойнее!"))
		return FALSE

	var/obj/structure/interactive/bed/resident/S = locate() in src.loc.contents
	if(!S)
		src.to_chat(span("danger","Мне нужно найти нормальное место для сна."))
		return FALSE

	if(!S.buckled)
		src.to_chat(span("danger","Стоя не спят!"))
		return FALSE

	return TRUE